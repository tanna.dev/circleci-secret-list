# CircleCI Secrets Lister

List the **names** of all secrets stored in CircleCI.

This does **not** list the secret values, only masked values.

Code to go alongside https://www.jvt.me/posts/2023/01/06/circleci-list-secrets/

## Usage

First, build the application with Go (v1.17+):

```sh
go build
# or
go install gitlab.com/tanna.dev/circleci-secret-list@HEAD
```

### Listing all contexts' secrets

By default, running:

```sh
export CIRCLE_TOKEN=...
./circleci-secret-list -token $CIRCLE_TOKEN -slug gh/jamietanna
```

Will output the contexts that exist, in a Tab Separated Value (TSV) format, for easy copy-paste into Google Sheets.

### Listing all projects' secrets

Prepare a list of all repositories in your organisation you want to scan, for instance via [this blog post](https://www.jvt.me/posts/2022/10/26/list-github-repos-org/).

Then run:

```sh
export CIRCLE_TOKEN=...
./circleci-secret-list -token $CIRCLE_TOKEN -slug gh/jamietanna -repos repos.txt
```

Will output the projects and any secrets that are configured, in a Tab Separated Value (TSV) format, for easy copy-paste into Google Sheets.
