package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/grezar/go-circleci"
)

func main() {
	var circleToken string
	var circleSlug string
	var reposFile string

	flag.StringVar(&circleToken, "token", "", "the CircleCI Personal API Token")
	flag.StringVar(&circleSlug, "slug", "", "the slug to your organisation on CircleCI, i.e. `gh/jamietanna`")
	flag.StringVar(&reposFile, "repos", "", "the path to the file on disk that has a list of repos to check, one per line. If present, will not print Contexts")

	flag.Parse()

	if circleToken == "" {
		errExit("No -token provided")
	}

	if circleSlug == "" {
		errExit("No -slug provided")
	}

	config := circleci.DefaultConfig()
	config.Token = circleToken

	client, err := circleci.NewClient(config)
	if err != nil {
		errExit("Could not construct the CircleCI Client: " + err.Error())
	}

	ctx := context.Background()

	if reposFile != "" {
		repos := fileToLines(reposFile)
		logProjectVariables(ctx, client, circleSlug, repos)
	} else {
		logContexts(ctx, client, circleSlug)
	}
}

func logContexts(ctx context.Context, client *circleci.Client, circleSlug string) {
	contexts, err := client.Contexts.List(ctx, circleci.ContextListOptions{
		OwnerSlug: circleci.String(circleSlug),
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not query Contexts: %v\n", err.Error())
		return
	}

	headers := []string{
		"Context Name",
		"Context ID",
		"Variable Name",
		"Created At",
	}
	fmt.Println(strings.Join(headers, "\t"))
	for _, context := range contexts.Items {
		variables, err := client.Contexts.ListVariables(ctx, context.ID)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to list environment variables for context %s: %v\n", context.Name, err.Error())
			continue
		}

		for _, cv := range variables.Items {
			elems := []string{
				context.Name,
				context.ID,
				cv.Variable,
				cv.CreatedAt.Format(time.RFC3339),
			}
			fmt.Println(strings.Join(elems, "\t"))
		}
	}
}

func logProjectVariables(ctx context.Context, client *circleci.Client, circleSlug string, repos []string) {
	headers := []string{
		"Repository Name",
		"Variable Name",
		"Masked Value",
	}
	fmt.Println(strings.Join(headers, "\t"))
	for _, repo := range repos {
		projectSlug := fmt.Sprintf("%s/%s", circleSlug, repo)
		logProjectVariablesForRepo(ctx, client, repo, projectSlug)
	}
}

func logProjectVariablesForRepo(ctx context.Context, client *circleci.Client, repo string, projectSlug string) {
	variables, err := client.Projects.ListVariables(ctx, projectSlug)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not query variables for slug %s: %v", projectSlug, err.Error())
		return
	}

	for _, variable := range variables.Items {
		elems := []string{
			repo,
			variable.Name,
			variable.Value,
		}
		fmt.Println(strings.Join(elems, "\t"))
	}
}

func errExit(err string, a ...any) {
	fmt.Fprintf(os.Stderr, err+"\n", a...)
	os.Exit(1)
}

func fileToLines(filePath string) []string {
	// adapted from https://golangdocs.com/golang-read-file-line-by-line
	readFile, err := os.Open(filePath)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not read file %s: %v\n", filePath, err.Error())
		return nil
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var fileLines []string

	for fileScanner.Scan() {
		fileLines = append(fileLines, fileScanner.Text())
	}

	readFile.Close()
	return fileLines
}
