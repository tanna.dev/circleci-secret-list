module gitlab.com/tanna.dev/circleci-secret-list

go 1.18

require github.com/grezar/go-circleci v0.8.0

require github.com/google/go-querystring v1.1.0 // indirect
